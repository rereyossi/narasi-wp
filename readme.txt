=== Saudagar ===
Contributors: Webforia Studio
Requires at least: 4.7
Tested up to: 5.1
Stable tag: 1.1.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
Tags: blog, news, gutenberg, newspaper, simple, elegant, minimalis

= 1.0.0 =
* First release

== Resources ==

Saudagar bundles the following third-party resources:

* [Kirki](http://aristath.github.io/kirki/)
* [Font Awesome Free](https://fontawesome.com)
* [Owl Carousel](https://github.com/OwlCarousel2/OwlCarousel2)
* [Merlin Wp](https://github.com/richtabor/MerlinWP)