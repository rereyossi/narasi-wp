<?php
$nextPost = get_next_post();
$prevPost = get_previous_post();
?>

<?php if(!empty($nextPost) || !empty($prevPost)): ?>
    <div class="rt-single-nav mb-30">

    <a href="<?php echo esc_url(get_the_permalink($prevPost->ID)) ?>" id="<?php echo 'post-related-'.$prevPost->ID?>" class="rt-single-nav__item rt-single-nav__prev">
    <?php if (!empty($prevPost)): ?>
        <div class="rt-single-nav__thumbnail rt-img rt-img--full"><?php echo get_the_post_thumbnail($prevPost->ID, 'thumbnail') ?></div>
        <div class="rt-single-nav__body">
            <h4 class="rt-single-nav__title"><?php echo rt_limited_string(get_the_title($prevPost->ID), 9) ?></h4>
            <div class="rt-single-nav__name "><i class="ti-arrow-left"></i><?php echo __('Previous Post', 'rt_domain') ?></div>
        </div>
    <?php endif?>
    </a>

    <a href="<?php echo esc_url(get_the_permalink($nextPost->ID)) ?>" id="<?php echo 'post-related-'.$nextPost->ID?>" class="rt-single-nav__item rt-single-nav__next">
    <?php if (!empty($nextPost)): ?>
        <div class="rt-single-nav__thumbnail rt-img rt-img--full"><?php echo get_the_post_thumbnail($nextPost->ID, 'thumbnail') ?></div>
        <div class="rt-single-nav__body">
            <h4 class="rt-single-nav__title"><?php echo rt_limited_string(get_the_title($nextPost->ID), 9) ?></h4>
            <div class="rt-single-nav__name"><?php echo __('Newer Post', 'rt_domain') ?><i class="ti-arrow-right"></i></div>
        </div>
    <?php endif?>
    </a>
</div>
<?php endif ?>