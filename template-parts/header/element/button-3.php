
<?php // Plan - Premium ?>
<?php
$target = '';
if (rt_option('header_button_3_link')) {
  $target = 'target="' . rt_option('header_button_3_link_target', 'blank') . '"';
}
?>
<div  class="rt-header__element rt-header__btn">
  <a href="<?php echo rt_option('header_button_3_link', '#') ?>" class="rt-btn rt-btn--3 mb-0" <?php echo $target ?>>

    <?php if (rt_option('header_button_3_icon')) : ?>
      <i class="<?php echo 'fa fa-' . rt_option('header_button_3_icon') ?>"></i>
    <?php endif; ?>

    <span><?php echo rt_option('header_button_3_text', 'Button 3') ?></span>
  </a>
</div>
