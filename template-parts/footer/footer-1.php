
<footer id="page-footer" <?php rt_set_class('rt_footer_class', ['page-footer page-footer--1']) ?>>

    <?php do_action('rt_footer_prepend')?>

    <?php if(rt_option('footer_widget', true)):?>

     <?php do_action('rt_before_footer_widget')?>

    <div class="page-footer__widget">
        <div class="page-container">

                <div <?php rt_set_class('rt_footer_widget_wrapper_class', ['flex flex-row flex-cols-md-3 flex-cols-sm-6 flex-cols-sm-12']) ?>>
            
                    <div id="widget-footer-1" class="flex-item">
                    <?php if ( is_active_sidebar( 'retheme_footer_1' ) ) :
                            dynamic_sidebar( 'retheme_footer_1' );
                        endif; ?>
                    </div>

                    <div id="widget-footer-2" class="flex-item">
                     <?php if ( is_active_sidebar( 'retheme_footer_2' ) ) :
                            dynamic_sidebar( 'retheme_footer_2' );
                        endif; ?>
                    </div>

                    <div id="widget-footer-3" class="flex-item">
                      <?php if ( is_active_sidebar( 'retheme_footer_3' ) ) :
                            dynamic_sidebar( 'retheme_footer_3' );
                        endif; ?>
                    </div>

                    <div id="widget-footer-4" class="flex-item">
                     <?php if ( is_active_sidebar( 'retheme_footer_4' ) ) :
                            dynamic_sidebar( 'retheme_footer_4' );
                        endif; ?>
                    </div>

               </div>

        </div>

    </div>
     <?php do_action('rt_after_footer_widget')?>

    <?php endif; ?>

    <?php rt_get_template_part('footer/footer-bottom');?>

     <?php do_action('rt_footer_append')?>

</footer>
