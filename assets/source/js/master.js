jQuery(document).ready(function($) {
  $("html")
    .removeClass("no-js")
    .addClass("js");

  $(".js-menu").retheme_menu();
  $(".js-tab").retheme_tabs();
  $(".js-search").retheme_search();
  $(".js-sidepanel").retheme_sidepanel();
  $(".js-modal").retheme_modal();
  $(".js-accordion").retheme_accordion();

  /*=================================================;
  /* MENU - Responsive desain on mobile
  /*================================================= */
  var main_menu = function() {
    var element = $(".js-main-menu-canvas");
    var trigger = $(".js-mobile-menu-trigger");

    trigger.on("click", function(event) {
      event.preventDefault();

      if ($(this).hasClass("is-active")) {
        $(this).removeClass("is-active");

        element.velocity("slideUp", {
          duration: "300",
          complete: function() {
            $(this).removeClass("is-active");
          }
        });
      } else {
        $(this).addClass("is-active");
        element.velocity("stop").velocity("slideDown", {
          duration: "300",
          complete: function() {
            $(this).addClass("is-active");
          }
        });
      }
    });
  };
  main_menu();

  /*=================================================
  *  HEADER
  =================================================== */

  var header = function() {
    var header_desktop = $(".js-header");
    var header_mobile = $(".js-header-mobile");

    /**
     * Check device if resize windows
     */
    $(window)
      .on("resize", function(e) {
        var windows = $(window).width();

        if (windows >= header_desktop.getData("responsive")) {
          header_desktop.show();
          header_mobile.hide();
        } else {
          header_desktop.hide();
          header_mobile.show();
        }
      })
      .trigger("resize");

    /* sticky  desktop*/
    $(window).scroll(function(event) {
      if ($(this).scrollTop() > 100 && header_desktop.getData("sticky") == true) {
        header_desktop.addClass("is-sticky");
      } else {
        header_desktop.removeClass("is-sticky");
      }
    });
     /**
     * Header mobile menu handle
     */
    $(window).scroll(function(event) {
      if ($(this).scrollTop() > 100 && header_mobile.getData("sticky") == true) {
        header_mobile.addClass("is-sticky");
      } else {
        header_mobile.removeClass("is-sticky");
      }
    });

   
  };

  header();

  /* =================================================
   *  MASONRY
   * =================================================== */
  var masonry = function(element) {
    if (jQuery().masonry) {
      // init Masonry
      var $grid = $(element).masonry({
        columnWidth: ".flex-item",
        itemSelector: ".flex-item"
      });

      // layout Masonry after each image loads
      $grid.imagesLoaded().progress(function() {
        $grid.masonry("layout").masonry({
          horizontalOrder: true
        });
      });
    }
  };

  masonry(".js-masonry");

  /*=================================================
  *  STICKY ASIDE
  =================================================== */

  var sticky = function() {
    if ($('body').hasClass('sticky-sidebar')) {
      var sticky_aside = $(".js-aside-sticky");

      $(window)
        .on("resize", function(e) {
          var windows = $(window).width();
          if (windows >= 992) {
            sticky_aside.stick_in_parent();
          } else {
            sticky_aside.trigger("sticky_kit:detach");
          }
        })
        .trigger("resize");
    }
  };

  sticky();

  var slider = function(element) {
    if (jQuery().owlCarousel) {
      $(element).each(function(index) {
        // get form slider
        var slider = $("#" + $(this).attr("id"));

        var slider_main = $("#" + $(this).attr("id")).find(".rt-slider__main");

        // disable class if not infinity slider
        if (slider.hasData("loop")) {
          slider.find(".js-slider-prev").addClass("is-disable");
        }

        slider_main.owlCarousel({
          loop: slider.getData("loop"),
          animateOut: slider.getData("animateout"),
          animateIn: slider.getData("animatein"),
          autoplay: slider.getData("autoplay"),
          lazyLoad: slider.getData("lazyload"),
          center: slider.getData("center"),
          margin: slider.getData("gap"),
          nav: slider.getData("nav"),
          dots: slider.getData("pagination"),
          stagePadding: slider.getData("padding"),
          autoplayHoverPause: slider.getData("autoplayhoverpause"),
          autoplaySpeed: slider.getData("autoplayspeed"),
          slideSpeed: 2000,
          navText: [
            "<i class='" + slider.getData("nav-icon-left") + "'></i>",
            "<i class='" + slider.getData("nav-icon-right") + "'></i>"
          ],
          responsive: {
            0: {
              items: slider.getData("items-sm")
            },
            720: {
              items: slider.getData("items-md")
            },
            960: {
              items: slider.getData("items-lg")
            }
          },
          onTranslated: function(element) {
            // disable nav header if first item or last item
            if (
              slider
                .find(".owl-item")
                .first()
                .hasClass("active")
            ) {
              slider.find(".js-slider-prev").addClass("is-disable");
              slider.find(".js-slider-next").removeClass("is-disable");
            }

            if (
              slider
                .find(".owl-item")
                .last()
                .hasClass("active")
            ) {
              slider.find(".js-slider-prev").removeClass("is-disable");
              slider.find(".js-slider-next").addClass("is-disable");
            }
          },
          onInitialized: function(element) {
            // hidden nav if only 1 page
            var pages = element.page.size; // Number of pages
            var items = element.item.count; // Number of items
            if (pages == items || pages > items) {
              slider.find(".rt-slider__nav").hide();
              slider.find(".rt-header-block__nav").hide();
            } else {
              slider.find(".rt-slider__nav").show();
            }
          }
        });

        /**
         * CUSTOM NAVIGATION ON HEADER
         */
        slider.find(".js-slider-prev").click(function(event) {
          slider.find(".rt-slider__main").trigger("prev.owl.carousel");
        });
        slider.find(".js-slider-next").click(function(event) {
          slider.find(".rt-slider__main").trigger("next.owl.carousel");
        });

        slider.css("display", "block");

        /** end each */
      });
    }
  };

  slider(".js-slider");

  /*=================================================;
 /* SLIDER SYNC
 /*================================================= */
  var slider_sync = function(element) {
    if (jQuery().owlCarousel) {
      $(element).each(function(index) {
        var slider = $("#" + $(this).attr("id"));

        var slider_1 = slider.find(".rt-slider__main");
        var slider_2 = slider.find(".rt-slider__group");
        var slidesPerPage = 4; //globaly define number of elements per page
        var syncedSecondary = true;

        slider_1
          .owlCarousel({
            items: 1,
            slideSpeed: 2000,

            nav: false,
            autoplay: true,
            dots: slider.getData("pagination"),
            loop: true,
            responsiveRefreshRate: 200
          })
          .on("changed.owl.carousel", syncPosition);

        slider_2
          .on("initialized.owl.carousel", function() {
            slider_2
              .find(".owl-item")
              .eq(0)
              .addClass("is-current");
          })
          .owlCarousel({
            dots: false,
            nav: slider.getData("nav"),
            smartSpeed: 200,
            slideSpeed: 500,
            slideBy: slidesPerPage, //alternatively you can slide by 1, this way the active slide will stick to the first item in the second carousel
            responsiveRefreshRate: 100,
            margin: slider.getData("gap"),
            navText: [
              "<i class='" + slider.getData("nav-icon-left") + "'></i>",
              "<i class='" + slider.getData("nav-icon-right") + "'></i>"
            ],
            responsive: {
              0: {
                items: slider.getData("items-sm")
              },
              720: {
                items: slider.getData("items-md")
              },
              960: {
                items: slider.getData("items-lg")
              }
            }
          })
          .on("changed.owl.carousel", syncPosition2);

        function syncPosition(el) {
          //if you set loop to false, you have to restore this next line
          //var current = el.item.index;

          //if you disable loop you have to comment this block
          var count = el.item.count - 1;
          var current = Math.round(el.item.index - el.item.count / 2 - 0.5);

          if (current < 0) {
            current = count;
          }
          if (current > count) {
            current = 0;
          }

          //end block

          slider_2
            .find(".owl-item")
            .removeClass("is-current")
            .eq(current)
            .addClass("is-current");
          var onscreen = slider_2.find(".owl-item.active").length - 1;
          var start = slider_2
            .find(".owl-item.active")
            .first()
            .index();
          var end = slider_2
            .find(".owl-item.active")
            .last()
            .index();

          if (current > end) {
            slider_2.data("owl.carousel").to(current, 100, true);
          }
          if (current < start) {
            slider_2.data("owl.carousel").to(current - onscreen, 100, true);
          }
        }

        function syncPosition2(el) {
          if (syncedSecondary) {
            var number = el.item.index;
            slider_1.data("owl.carousel").to(number, 100, true);
          }
        }

        slider_2.on("click", ".owl-item", function(e) {
          e.preventDefault();
          var number = $(this).index();
          slider_1.data("owl.carousel").to(number, 300, true);
        });
      });
    }
  };

  slider_sync(".js-slider-sync");

  /*=================================================
  *  Go To Top
  =================================================== */
  var gotop = function() {
    var gotop = $(".js-gotop");
    $(window).scroll(function(event) {
      if ($(this).scrollTop() > 80) {
        gotop.addClass("is-active");
      } else {
        gotop.removeClass("is-active");
      }
    });
    gotop.click(function(event) {
      $("body").velocity("scroll", {
        duration: 700
      });
    });
  };
  gotop();



  /*=================================================;
  /* SET ACTIVE
  /*================================================= */
  var toggle_active = function(element) {
    $(".js-active-toggle").each(function(index) {
      var action = $(this).getData("action");
      var animationIn = $(this).getData("animationIn");
      var animationDuration = $(this).getData("animationDuration");
      var element = $(this).attr("id");

      $(trigger).on("click", function() {
        if (element.hasClass("is-active")) {
          element.removeClass("is-active");
        } else {
          element
            .velocity("stop")
            .velocity(animationIn, {
              duration: animationDuration
            })
            .addClass("is-active");
        }
      });
    });
  };
  toggle_active();
  /*=================================================
  *  Comments
  =================================================== */
  // Show all form comments for single post

  var comments = function(elements) {
    var element = $(elements);
    element.find("textarea").on("click, focus", function(event) {
      event.preventDefault();
      if (!element.hasClass("is-active")) {
        $(element)
          .find(".comment-input")
          .velocity("stop")
          .velocity("slideDown", {
            duration: "600",
            display: "block"
          });

        element.addClass("is-active");
      }
    });
  };

  comments(".js-comment");

  /*=================================================
  *  ELEMENTOR REGISTER
  =================================================== */

  var elementor_frontend_hook = function() {
    elementorFrontend.hooks.addAction(
      "frontend/element_ready/retheme-product.default",
      function() {
        slider(".js-elementor-slider");
        masonry(".js-elementor-masonry");
      }
    );

    elementorFrontend.hooks.addAction(
      "frontend/element_ready/retheme-post.default",
      function() {
        slider(".js-elementor-slider");
        masonry(".js-elementor-masonry");
      }
    );

    elementorFrontend.hooks.addAction(
      "frontend/element_ready/retheme-carousel.default",
      function() {
        slider(".js-elementor-slider");
        masonry(".js-elementor-masonry");
      }
    );
  };

  if ($("body").hasClass("elementor-editor-active")) {
    elementor_frontend_hook();
  } else {
    $(window).on("elementor/frontend/init", function(options) {
      elementor_frontend_hook();
    });
  }

  // end document
});
