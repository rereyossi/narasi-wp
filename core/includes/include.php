<?php
include_once dirname(__FILE__) . '/inc-setup.php';
include_once dirname(__FILE__) . '/inc-option.php';
include_once dirname(__FILE__) . '/inc-helper.php';
include_once dirname(__FILE__) . '/inc-image.php';
include_once dirname(__FILE__) . '/inc-template-part.php';
include_once dirname(__FILE__) . '/inc-template-tag.php';

include_once dirname(__FILE__) . '/inc-script.php';

include_once dirname(__FILE__) . '/inc-archive.php';
include_once dirname(__FILE__) . '/inc-post.php';
include_once dirname(__FILE__) . '/inc-page.php';
include_once dirname(__FILE__) . '/inc-comment.php';

include_once dirname(__FILE__) . '/inc-widget.php';
include_once dirname(__FILE__) . '/inc-menu.php';



