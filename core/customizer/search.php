<?php
/**
 * @author : Reret
 */
namespace Retheme\Customizer;

use Retheme\Customizer_Base;

class Search extends Customizer_Base
{

    public function __construct()
    {

        $this->set_section();

        $this->add_search_option();
    }

    public function set_section()
    {
        $this->add_section('', array(
            'search_option' => array(esc_attr__('Search ', 'rt_domain')),
        ));
    }

    public function add_search_option()
    {
        $section = 'search_option_section';
        $settings = 'search_options';

        $this->add_field(array(
            'type' => 'text',
            'section' => $section,

            'settings' => $settings . '_text',
            'label' => __('Placeholder', 'rt_domain'),
            'default' => 'Type Something and enter',
        ));

        if ( rt_is_premium()) {
            $this->add_field_color(array(
                'settings' => $settings . '_color',
                'section' => $section,
                'element' => '.rt-search .rt-search__input,
						 .rt-search .rt-search__icon',
            ));

            $this->add_field_background(array(
                'settings' => $settings . '_background',
                'section' => $section,
                'element' => '.rt-search',
            ));

            $this->add_field_border_color(array(
                'settings' => $settings . '_border',
                'section' => $section,
                'element' => '.rt-search',
            ));

        }
    }

// end class
}

new Search;
