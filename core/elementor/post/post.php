<?php
namespace Retheme\Elementor;

use Retheme\Elementor_Base;
use Retheme\Helper;
use Elementor\Controls_Manager;
use Elementor\Group_Control_Border;
use Elementor\Group_Control_Typography;

if (!defined('ABSPATH')) {
    exit;
} // Exit if accessed directly

class Posts extends Elementor_Base
{
    protected $post_type = 'post';
    protected $post_taxonomy = 'category';

    public function get_name()
    {
        return 'retheme-post';
    }

    public function get_title()
    {
        return __('Posts Grid', 'rt_domain');
    }

    public function get_icon()
    {
        return 'ate-icon ate-post';
    }

    public function get_categories()
    {
        return ['retheme-elements'];
    }

    protected function _register_controls()
    {
        $this->setting_header_block();
        $this->setting_query();
        $this->setting_options(); //protected

        $this->style_general(); //protected
        $this->style_body();
        $this->style_title();
        $this->style_badges();
        $this->style_meta();
        $this->style_excerpt();

        $this->setting_button(array(
            'name' => 'readmore',
            'class' => '.rt-post__readmore',
            'label' => 'Read More',
        ));

        $this->setting_button(array(
            'name' => 'loadmore',
            'label' => 'Load More',
            'class' => '.rt-pagination__button',
        ));

        $this->setting_carousel();
    }

    /**
     *  query post
     * @return [query section]
     */
    public function setting_query($args = array())
    {
        $this->start_controls_section(
            'setting_query',
            [
                'label' => __('Query', 'rt_domain'),
            ]
        );

        /**
         *  Number perpage not show if widget smart-tiles
         */
        if (empty($args['limit_perpage'])) {
            $this->add_control(
                'posts_per_page',
                [
                    'label' => __('Posts Number', 'rt_domain'),
                    'type' => Controls_Manager::NUMBER,
                    'default' => 6,
                ]
            );
            $this->add_control(
                'advanced',
                [
                    'label' => __('Advanced', 'rt_domain'),
                    'type' => Controls_Manager::HEADING,
                ]
            );
        } else {
            $this->add_control(
                'advanced',
                [
                    'label' => __('Advanced', 'rt_domain'),
                    'type' => Controls_Manager::HEADING,
                    'separator' => 'before',
                ]
            );
        }

        $this->add_control(
            'query_by',
            [
                'label' => __('Query posts by', 'rt_domain'),
                'type' => Controls_Manager::SELECT,
                'default' => 'lastest',
                'options' => [
                    'lastest' => __('Lastest Posts', 'rt_domain'),
                    'category' => __('Category', 'rt_domain'),
                    'manually' => __('Manually', 'rt_domain'),
                ],
            ]
        );

        $this->add_control(
            'category',
            [
                'label' => __('Categories', 'rt_domain'),
                'type' => Controls_Manager::SELECT2,
                'multiple' => true,
                'options' => Helper::get_terms($this->post_taxonomy),
                'condition' => [
                    'query_by' => 'category',
                ],
            ]
        );
        $this->add_control(
            'post_id',
            [
                'label' => __('Select Post', 'rt_domain'),
                'type' => Controls_Manager::SELECT2,
                'multiple' => true,
                'options' => Helper::get_posts($this->post_type),
                'condition' => [
                    'query_by' => 'manually',
                ],

            ]
        );

        $this->add_control(
            'orderby',
            [
                'label' => __('Order By', 'rt_domain'),
                'type' => Controls_Manager::SELECT,
                'default' => 'date',
                'options' => [
                    'ID' => 'Post Id',
                    'author' => 'Post Author',
                    'title' => 'Title',
                    'date' => 'Date',
                    'modified' => 'Last Modified Date',
                    'parent' => 'Parent Id',
                    'rand' => 'Random',
                    'comment_count' => 'Comment Count',
                    'menu_order' => __('Menu Order', 'rt_domain'),
                    'wp_post_views_count' => __('Most Viewer', 'rt_domain'),
                    'comment_count' => __('Most Review', 'rt_domain'),
                ],
            ]
        );

        $this->add_control(
            'order',
            [
                'label' => __('Order', 'rt_domain'),
                'type' => Controls_Manager::SELECT,
                'default' => 'DESC',
                'options' => [
                    'ASC' => __('ASC', 'rt_domain'),
                    'DESC' => __('DESC', 'rt_domain'),
                ],
            ]
        );

        $this->add_control(
            'offset',
            [
                'label' => __('Offset', 'rt_domain'),
                'type' => Controls_Manager::NUMBER,
                'default' => 0,
                'description' => __('Use this setting to skip over posts (e.g. \'2\' to skip over 2 posts).', 'rt_domain'),
            ]
        );

        $this->end_controls_section();
    }

    protected function setting_options()
    {
        $this->start_controls_section(
            'setting_option',
            [
                'label' => __('Options', 'rt_domain'),
            ]
        );

        $this->add_responsive_control(
            'setting_column',
            [
                'label' => __('Column', 'rt_domain'),
                'type' => Controls_Manager::SELECT,
                'options' => [
                    1 => __(1, 'rt_domain'),
                    2 => __(2, 'rt_domain'),
                    3 => __(3, 'rt_domain'),
                    4 => __(4, 'rt_domain'),
                    6 => __(6, 'rt_domain'),
                ],
                'devices' => ['desktop', 'tablet', 'mobile'],
                'desktop_default' => 3,
                'tablet_default' => 2,
                'mobile_default' => 1,
                'condition' => [
                    'carousel!' => 'yes',
                ],

            ]
        );

        $this->add_control(
            'layout_masonry',
            [
                'label' => __('Masonry', 'rt_domain'),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'no',
                'label_off' => __('Off', 'rt_domain'),
                'label_on' => __('On', 'rt_domain'),
                'return_value' => 'yes',
                'condition' => [
                    'carousel!' => 'yes',
                ],
            ]
        );

        $this->add_control(
            'image_size',
            [
                'label' => __('Image Size', 'rt_domain'),
                'type' => Controls_Manager::SELECT,
                'default' => 'medium',
                'options' => Helper::get_image_size(),
            ]
        );

        $this->add_control(
            'excerpt',
            [
                'label' => __('Excerpt Number', 'rt_domain'),
                'type' => Controls_Manager::NUMBER,
                'default' => __('18', 'rt_domain'),
            ]
        );

        $this->add_control(
            'readmore',
            [
                'label' => __('Read More', 'rt_domain'),
                'type' => Controls_Manager::TEXT,
                'default' => __('Read More', 'rt_domain'),
            ]
        );

        $this->add_control(
            'meta_category',
            [
                'label' => __('Category', 'rt_domain'),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'yes',
                'label_on' => __('On', 'rt_domain'),
                'label_off' => __('Off', 'rt_domain'),
                'return_value' => 'yes',
            ]
        );

        $this->add_control(
            'meta_date',
            [
                'label' => __('Date', 'rt_domain'),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'yes',
                'label_on' => __('On', 'rt_domain'),
                'label_off' => __('Off', 'rt_domain'),
                'return_value' => 'yes',
            ]
        );

        $this->add_control(
            'meta_author',
            [
                'label' => __('Author', 'rt_domain'),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'no',
                'label_on' => __('On', 'rt_domain'),
                'label_off' => __('Off', 'rt_domain'),
                'return_value' => 'yes',
            ]
        );

        // $this->add_control(
        //     'meta_view',
        //     [
        //         'label' => __('View Count', 'rt_domain'),
        //         'type' => Controls_Manager::SWITCHER,
        //         'default' => 'no',
        //         'label_on' => __('On', 'rt_domain'),
        //         'label_off' => __('Off', 'rt_domain'),
        //         'return_value' => 'yes',
        //     ]
        // );

        $this->add_control(
            'meta_comment',
            [
                'label' => __('Comment Count', 'rt_domain'),
                'type' => Controls_Manager::SWITCHER,
                'default' => 'no',
                'label_on' => __('On', 'rt_domain'),
                'label_off' => __('Off', 'rt_domain'),
                'return_value' => 'yes',
            ]
        );

        $this->add_control(
            'pagination_style',
            [
                'label' => __('Pagination Style', 'rt_domain'),
                'type' => Controls_Manager::SELECT,
                'default' => 'no_pagination',
                'options' => array(
                    'no_pagination' => 'No Pagination',
                    'loadmore' => 'Load More',
                ),
            ]
        );


        $this->end_controls_section();
    }



    protected function style_general()
    {
        $this->start_controls_section(
            'style_general',
            [
                'label' => __('General', 'rt_domain'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_responsive_control(
            'general_column_gap',
            [
                'label' => __('Column Gap', 'rt_domain'),
                'type' => Controls_Manager::SLIDER,
                'range' => [
                    'px' => [
                        'min' => 0,
                        'max' => 30,
                        'step' => 1,
                    ],

                ],
                'size_units' => ['px'],
                'selectors' => [
                    '{{WRAPPER}} .flex-loop' => 'margin-left: calc(-{{SIZE}}{{UNIT}}/2);
                                                margin-right: calc(-{{SIZE}}{{UNIT}}/2);',
                    '{{WRAPPER}} .flex-loop > .flex-item' => 'padding-left: calc({{SIZE}}{{UNIT}}/2);
                                                    padding-right: calc({{SIZE}}{{UNIT}}/2);
                                                    margin-bottom: {{SIZE}}{{UNIT}};',
                ],
                'condition' => [
                    'carousel!' => 'yes',
                ],
            ]
        );

        $this->add_responsive_control(
            'general_alignment',
            [
                'label' => __('Layout Alignment', 'rt_domain'),
                'type' => Controls_Manager::CHOOSE,
                'options' => [
                    'left' => [
                        'title' => __('Left', 'rt_domain'),
                        'icon' => 'fa fa-align-left',
                    ],
                    'center' => [
                        'title' => __('Center', 'rt_domain'),
                        'icon' => 'fa fa-align-center',
                    ],
                    'right' => [
                        'title' => __('Right', 'rt_domain'),
                        'icon' => 'fa fa-align-right',
                    ],
                ],
                'selectors' => [
                    '{{WRAPPER}} .rt-post__title,
                    {{WRAPPER}} .rt-badges,
                    {{WRAPPER}} .rt-post__meta,
                    {{WRAPPER}} .rt-post__body' => 'text-align: {{VALUE}};',
                ],

            ]
        );

        $this->add_group_control(
            Group_Control_Border::get_type(),
            [
                'name' => 'general_border',
                'selector' => '{{WRAPPER}} .rt-post',
            ]
        );

        $this->add_responsive_control(
            'general_radius',
            [
                'label' => __('Border Radius', 'rt_domain'),
                'type' => Controls_Manager::SLIDER,
                'range' => [
                    'px' => [
                        'min' => 0,
                        'max' => 80,
                        'step' => 1,
                    ],

                ],
                'size_units' => ['px'],
                'selectors' => [
                    '{{WRAPPER}} .rt-post' => 'border-radius: {{SIZE}}{{UNIT}};',
                ],
            ]
        );

        $this->end_controls_section();
    }

    public function style_image()
    {
        $this->start_controls_section(
            'style_image',
            [
                'label' => __('Image', 'rt_domain'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );
        $this->add_responsive_control(
            'image_width',
            [
                'label' => __('Image Width', 'rt_domain'),
                'type' => Controls_Manager::SLIDER,
                'range' => [
                    'px' => [
                        'min' => 80,
                        'max' => 200,
                        'step' => 1,
                    ],
                    '%' => [
                        'min' => 10,
                        'max' => 100,
                        'step' => 1,
                    ],

                ],
                'size_units' => ['px', '%'],
                'selectors' => [
                    '{{WRAPPER}} .rt-post__thumbnail' => 'width: {{SIZE}}{{UNIT}};',
                ],

            ]
        );
        $this->end_controls_section();

    }

    public function style_body()
    {

        $this->start_controls_section(
            'style_body',
            [
                'label' => __('Body', 'rt_domain'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'body_background',
            [
                'label' => __('Background Color', 'rt_domain'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .rt-post' => 'background-color: {{VALUE}};',
                ],
            ]
        );

        $this->add_responsive_control(
            'body_padding',
            [
                'label' => __('Padding', 'rt_domain'),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => ['px'],
                'selectors' => [
                    '{{WRAPPER}} .rt-post__body' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
            ]

        );

        $this->end_controls_section();

    }

    public function style_title()
    {
        $this->start_controls_section(
            'style_title',
            [
                'label' => __('Title', 'rt_domain'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        /* start title color */
        $this->start_controls_tabs('title_tabs');
        $this->start_controls_tab(
            'title_normal',
            [
                'label' => __('Normal', 'rt_domain'),
            ]
        );
        $this->add_control(
            'title_color',
            [
                'label' => __('Color', 'rt_domain'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .rt-post__title a' => 'color: {{VALUE}};',
                ],
                'separator' => 'after',
            ]
        );

        $this->end_controls_tab();

        $this->start_controls_tab(
            'title_hover',
            [
                'label' => __('Hover', 'rt_domain'),
            ]
        );
        $this->add_control(
            'title_color_hover',
            [
                'label' => __('Color', 'rt_domain'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .rt-post__title a:hover' => 'color: {{VALUE}};',
                ],
                'separator' => 'after',
            ]
        );

        $this->end_controls_tab();
        $this->end_controls_tabs();
        /* end title color */

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'title_typography',
                'selector' => '{{WRAPPER}} .rt-post__title',
            ]
        );

        $this->add_responsive_control(
            'title_margin',
            [
                'label' => __('Title Spacing', 'rt_domain'),
                'type' => Controls_Manager::SLIDER,
                'size_units' => ['px', '%'],
                'selectors' => [
                    '{{WRAPPER}} .rt-post__title' => 'margin-bottom: {{SIZE}}{{UNIT}};',
                ],
            ]

        );

        $this->end_controls_section();
    }

    public function style_badges()
    {
        $this->start_controls_section(
            'style_badges',
            [
                'label' => __('Badges', 'rt_domain'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        /* start title color */
        $this->start_controls_tabs('badges_tabs');
        $this->start_controls_tab(
            'badges_normal',
            [
                'label' => __('Normal', 'rt_domain'),
            ]
        );
        $this->add_control(
            'badges_color',
            [
                'label' => __('Color', 'rt_domain'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .rt-badges a' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_control(
            'badges_background',
            [
                'label' => __('Background Color', 'rt_domain'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .rt-badges a' => 'background-color: {{VALUE}}!important;',
                ],
            ]
        );

        $this->end_controls_tab();

        $this->start_controls_tab(
            'badges_hover',
            [
                'label' => __('Hover', 'rt_domain'),
            ]
        );
        $this->add_control(
            'badges_color_hover',
            [
                'label' => __('Color', 'rt_domain'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .rt-badges a:hover' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_control(
            'badges_background_hover',
            [
                'label' => __('Background Color', 'rt_domain'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .rt-badges a:hover' => 'background-color: {{VALUE}} !important;',
                ],
            ]
        );

        $this->end_controls_tab();
        $this->end_controls_tabs();

        $this->add_responsive_control(
            'badges_padding',
            [
                'label' => __('Padding', 'rt_domain'),
                'type' => Controls_Manager::DIMENSIONS,
                'size_units' => ['px'],
                'selectors' => [
                    '{{WRAPPER}} .rt-badges a' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
                ],
                'separator' => 'before',
            ]

        );

        $this->add_responsive_control(
            'badges_radius',
            [
                'label' => __('Badges Radius', 'rt_domain'),
                'type' => Controls_Manager::SLIDER,
                'default' => [
                    'size' => 3,
                    'unit' => 'px',
                ],
                'range' => [
                    'px' => [
                        'min' => 0,
                        'max' => 30,
                        'step' => 1,
                    ],
                ],
                'size_units' => ['px'],
                'selectors' => [
                    '{{WRAPPER}} .rt-badges a' => 'border-radius: {{SIZE}}{{UNIT}};',
                ],

            ]
        );

        $this->end_controls_section();
    }

    public function style_meta()
    {
        $this->start_controls_section(
            'style_meta',
            [
                'label' => __('Meta', 'rt_domain'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'meta_color',
            [
                'label' => __('Color', 'rt_domain'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .rt-post__meta-item' => 'color: {{VALUE}};',
                ],
            ]
        );

        /* start title color */
        $this->start_controls_tabs('meta_tabs');
        $this->start_controls_tab(
            'meta_normal',
            [
                'label' => __('Normal', 'rt_domain'),
            ]
        );

        $this->add_control(
            'meta_link',
            [
                'label' => __('Primary Link', 'rt_domain'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .rt-post__meta a' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->end_controls_tab();

        $this->start_controls_tab(
            'meta_hover',
            [
                'label' => __('Hover', 'rt_domain'),
            ]
        );
        $this->add_control(
            'meta_link_hover',
            [
                'label' => __('Primary Link', 'rt_domain'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .rt-post__meta a:hover' => 'color: {{VALUE}};',
                ],
            ]
        );
        $this->end_controls_tab();
        $this->end_controls_tabs();
        /* end title color */

        // $this->add_group_control(
        //     Group_Control_Typography::get_type(),
        //     [
        //         'name' => 'meta_typography',
        //         'selector' => '{{WRAPPER}} .rt-post__meta-item',
        //         'separator' => 'before',
        //     ]
        // );

        $this->add_control(
            'meta_color_divender',
            [
                'label' => __('Color Divender', 'rt_domain'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .rt-post__meta-item::before' => 'background: {{VALUE}};',
                ],
            ]
        );

        $this->add_responsive_control(
            'meta_margin',
            [
                'label' => __('Meta Spacing', 'rt_domain'),
                'type' => Controls_Manager::SLIDER,
                'size_units' => ['px', '%'],
                'selectors' => [
                    '{{WRAPPER}} .rt-post__meta' => 'margin-bottom: {{SIZE}}{{UNIT}};',
                ],
            ]

        );

        $this->end_controls_section();
    }

    public function style_excerpt()
    {
        $this->start_controls_section(
            'style_excerpt',
            [
                'label' => __('Excerpt', 'rt_domain'),
                'tab' => Controls_Manager::TAB_STYLE,
            ]
        );

        $this->add_control(
            'excerpt_color',
            [
                'label' => __('Color', 'rt_domain'),
                'type' => Controls_Manager::COLOR,
                'selectors' => [
                    '{{WRAPPER}} .rt-post__content' => 'color: {{VALUE}};',
                ],
            ]
        );

        $this->add_group_control(
            Group_Control_Typography::get_type(),
            [
                'name' => 'excerpt_typography',
                'selector' => '{{WRAPPER}} .rt-post__content',
            ]
        );

        $this->add_responsive_control(
            'excerpt_margin',
            [
                'label' => __('Excerpt Spacing', 'rt_domain'),
                'type' => Controls_Manager::SLIDER,
                'size_units' => ['px', '%'],
                'selectors' => [
                    '{{WRAPPER}} .rt-post__content' => 'margin-bottom: {{SIZE}}{{UNIT}};',
                ],
            ]

        );

        $this->end_controls_section();
    }

    protected function render()
    {
        $settings = $this->get_settings();
       
        $args = array(
            'post_type' => 'post',
            'id' => $this->get_id(),
            'template_part' => 'core/elementor/post/post-view',
        );


        echo $this->elementor_loop(wp_parse_args($args, $settings));

    }
    /* end class */
}
